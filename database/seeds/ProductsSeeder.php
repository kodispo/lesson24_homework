<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'title' => 'iPhone 11 Pro',
                'slug' => 'iphone-11-pro',
                'category_id' => 1,
                'price' => 1307.65,
                'description' => 'A transformative triple‑camera system that adds tons of capability without complexity. An unprecedented leap in battery life. And a mind‑blowing chip that doubles down on machine learning and pushes the boundaries of what a smartphone can do. Welcome to the first iPhone powerful enough to be called Pro.',
            ],
            [
                'title' => 'MacBook Pro 16 inch',
                'slug' => 'macbook-pro-16-inch',
                'category_id' => 2,
                'price' => 3499.96,
                'description' => 'Designed for those who defy limits and change the world, the new MacBook Pro is by far the most powerful notebook we’ve ever made. With an immersive 16-inch Retina display, superfast processors, next-generation graphics, the largest battery capacity ever in a MacBook Pro, a new Magic Keyboard, and massive storage, it’s the ultimate pro notebook for the ultimate user.',
            ],
            [
                'title' => 'Apple Watch Series 5',
                'slug' => 'apple-watch-series-5',
                'category_id' => 3,
                'price' => 523.03,
                'description' => 'This watch has a display that never sleeps. With the Always-On Retina display, you always see the time and your watch face. Get a quick read on your heart rate, or check your heart rhythm with the ECG app. The Noise app alerts you when decibels rise to levels that can impact your hearing.',
            ],
        ]);
    }
}
