<div class="bg-white border-bottom box-shadow">
    <div class="container d-flex flex-column flex-md-row align-items-center p-3 px-md-4">
        <h5 class="my-0 mr-md-auto font-weight-normal"><a class="text-dark logo" href="/">Lesson 24</a></h5>
        <nav class="my-2 my-md-0 mr-md-3">
            <a class="p-2 text-dark" href="/">Products</a>
            <a class="p-2 text-dark" href="/categories/">Categories</a>
            <a class="p-2 text-dark" href="/pages/">Pages</a>
        </nav>
    </div>
</div>
