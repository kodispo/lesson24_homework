<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lessons 24 homework</title>

    @include('partials.head-connects')
</head>
<body>

@include('partials.header')

<div class="bg-light">
    <div class="container">
        <div class="row">
            <div class="col py-5 text-center">
                @yield('hero')
            </div>
        </div>
    </div>
</div>

<div class="container my-5">
    <div class="row">
        <div class="col">
            @yield('content')
        </div>
    </div>
</div>

</body>
</html>
