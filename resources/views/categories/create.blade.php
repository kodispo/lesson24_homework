@extends('layout.base')

@section('hero')
    <h1 class="text-uppercase">Add new category</h1>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-lg-6">
            @include('partials.errors')
            <form action="/categories" method="post">
                @csrf
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" name="name" id="name" value="{{ old('name') }}" class="form-control">
                </div>

                <div class="form-group">
                    <label for="slug">Slug:</label>
                    <input type="text" name="slug" id="slug" value="{{ old('slug') }}" class="form-control">
                </div>

                <div class="form-group">
                    <button class="btn btn-success">Add</button>
                </div>
            </form>
        </div>
    </div>
@endsection
