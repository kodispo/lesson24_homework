@extends('layout.base')

@section('hero')
    <h1 class="text-uppercase text-center">Categories</h1>
    <a href="/categories/create" class="btn btn-outline-primary">Add new category</a>
@endsection

@section('content')
    @include('partials.alert')

    @if($categories)
        <div class="card-deck mb-3 text-center">
        @foreach($categories as $category)
            <div class="card mb-4 box-shadow">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">{{ $category->name }}</h4>
                </div>
                <div class="card-body">
                    <div class="d-flex flex-md-row justify-content-center">
                        <a href="/categories/{{ $category->slug }}" class="btn btn-sm btn-block m-0 btn-outline-primary">View</a>
                        <a href="/categories/{{ $category->slug }}/edit" class="btn btn-sm btn-block m-0 btn-outline-primary">Edit</a>
                        <form action="/categories/{{$category->slug}}" method="post" class="d-block">
                            @csrf
                            @method('delete')
                            <button class="btn btn-sm btn-block m-0 btn-outline-primary">Remove</button>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    @endif
@endsection
