@extends('layout.base')

@section('hero')
    <h1 class="text-uppercase">{{ $category->name }}</h1>
@endsection

@section('content')
    @if($category->products->count())
        <h3 class="mb-3">Related products:</h3>
        <ul class="list-group">
            @foreach($category->products as $product)
                <li class="list-group-item">
                    <a href="/products/{{ $product->slug }}">{{ $product->title }}</a>
                </li>
            @endforeach
        </ul>
    @else
        <p class="text-danger">No related products yet.</p>
    @endif
@endsection
