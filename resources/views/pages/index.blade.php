@extends('layout.base')

@section('hero')
    <h1 class="text-uppercase text-center">Pages</h1>
    <a href="/pages/create" class="btn btn-outline-primary">Add new page</a>
@endsection

@section('content')
    @include('partials.alert')

    @if($pages)
        <div class="card-deck mb-3 text-center">
        @foreach($pages as $page)
            <div class="card mb-4 box-shadow">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal">{{ $page->title }}</h4>
                </div>
                <div class="card-body">
                    <div class="mb-3">{{ \Illuminate\Support\Str::limit($page->intro, 130, $end='...') }}</div>
                    <div class="d-flex flex-md-row justify-content-center">
                        <a href="/pages/{{ $page->slug }}" class="btn btn-sm btn-block m-0 btn-outline-primary">View</a>
                        <a href="/pages/{{ $page->slug }}/edit" class="btn btn-sm btn-block m-0 btn-outline-primary">Edit</a>
                        <form action="/pages/{{$page->slug}}" method="post" class="d-block">
                            @csrf
                            @method('delete')
                            <button class="btn btn-sm btn-block m-0 btn-outline-primary">Remove</button>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    @endif
@endsection
