<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\Product\StoreProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;
use App\Product;

class ProductsController extends Controller
{
    public function index()
    {
        $products = Product::all();
        return view('products.index')->with(compact('products'));
    }

    public function show(Product $product)
    {
        return view('products.show')->with(compact('product'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('products.create')->with(compact('categories'));
    }

    public function store(StoreProductRequest $request)
    {
        Product::create($request->all());
        return redirect('/')->with('success', 'Created successfully');
    }

    public function edit(Product $product)
    {
        $categories = Category::all();
        return view('products.edit', compact(['product', 'categories']));
    }

    public function update(Product $product, UpdateProductRequest $request)
    {
        $product->update($request->all());
        return redirect('/')->with('success', 'Updated successfully');
    }

    public function destroy(Product $product)
    {
        $product->delete();
        return redirect('/')->with('success', 'Removed successfully');
    }
}
