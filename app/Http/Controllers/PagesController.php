<?php

namespace App\Http\Controllers;

use App\Http\Requests\Page\StorePageRequest;
use App\Http\Requests\Page\UpdatePageRequest;
use App\Page;

class PagesController extends Controller
{
    public function index()
    {
        $pages = Page::all();
        return view('pages.index')->with(compact('pages'));
    }

    public function show(Page $page)
    {
        return view('pages.show')->with(compact('page'));
    }

    public function create()
    {
        return view('pages.create');
    }

    public function store(StorePageRequest $request)
    {
        Page::create($request->all());
        return redirect('/pages/')->with('success', 'Created successfully');
    }

    public function edit(Page $page)
    {
        return view('pages.edit', compact('page'));
    }

    public function update(Page $page, UpdatePageRequest $request)
    {
        $page->update($request->all());
        return redirect('/pages/')->with('success', 'Updated successfully');
    }

    public function destroy(Page $page)
    {
        $page->delete();
        return redirect('/pages/')->with('success', 'Removed successfully');
    }
}
